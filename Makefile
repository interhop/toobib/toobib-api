.DEFAULT_GOAL := help
.PHONY := help venv deps dev test doc clean

all: clean db venv deps test dev	## execute clean, db, venv, deps, test and dev make commands


clean: 					## clean previous installation
	cd database/ && make drop-db
	rm -rdf venv
	
db: 					## install the database
	cd database/ && make create-db

drop-db: 				## drop the database
	cd database/ && make drop-db

help:					## Show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

venv: 					## Install the virtualenv
	virtualenv venv 

deps: 					## Install dependencies and config.py
	. venv/bin/activate && pip install -r app-flask/requirements.txt
	python3 build-config-file.py

dev: 					## Run the API in dev mode
	. venv/bin/activate && FLASK_APP=app-flask/server FLASK_RUN_PORT=5001 FLASK_DEBUG=True flask run

test: 					## Run tests with coverage
	cd database/ && make drop-test-db
	cd database/ && make create-test-db
	. venv/bin/activate && coverage run -m pytest
	cd database/ && make drop-test-db

doc: 					## Build documentation
	mkdocs build
	mkdocs serve
