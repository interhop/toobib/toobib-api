# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    Makefile                                           :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/21 20:01:00 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/02/18 18:07:35 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

USER=toobib
USER_EXTENSION=toobib_extension
DATABASE_NAME=toobib
TOOBIB_SCHEMA=internal_toobib
PG_CONF_NO_DB=host=localhost dbname=postgres user=$(USER)
PG_CONF=host=localhost dbname=$(DATABASE_NAME) user=$(USER)
PG_CONF_EXTENSION=host=localhost dbname=$(DATABASE_NAME) user=$(USER_EXTENSION)
TOOBIB_DATABASE_ARGUMENTS=$(PG_CONF) options=--search_path=$(TOOBIB_SCHEMA)


TEST_DATABASE_NAME=toobib_test
TEST_PG_CONF=host=localhost dbname=$(TEST_DATABASE_NAME) user=$(USER)
TEST_PG_CONF_EXTENSION=host=localhost dbname=$(TEST_DATABASE_NAME) user=$(USER_EXTENSION)
TEST_PG_CONF_NO_DB=host=localhost user=$(USER)
TEST_TOOBIB_DATABASE_ARGUMENTS=$(TEST_PG_CONF) options=--search_path=$(TOOBIB_SCHEMA)

all: create-db

create-db:
	psql "$(PG_CONF_NO_DB)" -c 'CREATE DATABASE "$(DATABASE_NAME)" OWNER "$(USER)";'	
	psql "$(PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS unaccent  with schema public;'
	psql "$(PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"  with schema public;'
	psql "$(PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS pg_trgm with schema public;'
	psql "$(PG_CONF)" -c 'DROP SCHEMA IF EXISTS "$(TOOBIB_SCHEMA)" CASCADE;'
	psql "$(PG_CONF)" -c 'DROP SCHEMA IF EXISTS "$(TOOBIB_SCHEMA)" CASCADE;'
	psql "$(PG_CONF)" -c 'CREATE SCHEMA "$(TOOBIB_SCHEMA)";'
	psql "$(TOOBIB_DATABASE_ARGUMENTS)" -f "toobib-create-table.sql"
	psql "$(TOOBIB_DATABASE_ARGUMENTS)" -f "toobib-constraints.sql"

drop-db:
	psql "$(PG_CONF_NO_DB)" -c "DROP DATABASE IF EXISTS $(DATABASE_NAME);"

create-test-db:
	psql "$(TEST_PG_CONF_NO_DB)" -c 'CREATE DATABASE "$(TEST_DATABASE_NAME)" OWNER "$(USER)";'	
	psql "$(TEST_PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS unaccent  with schema public;'
	psql "$(TEST_PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"  with schema public;'
	psql "$(TEST_PG_CONF_EXTENSION)" -c 'CREATE EXTENSION IF NOT EXISTS pg_trgm with schema public;'
	
	psql "$(TEST_PG_CONF)" -c 'DROP SCHEMA IF EXISTS "$(TOOBIB_SCHEMA)" CASCADE;'
	psql "$(TEST_PG_CONF)" -c 'CREATE SCHEMA "$(TOOBIB_SCHEMA)";'
	psql "$(TEST_TOOBIB_DATABASE_ARGUMENTS)" -f "toobib-create-table.sql"
	psql "$(TEST_TOOBIB_DATABASE_ARGUMENTS)" -f "toobib-constraints.sql"
	psql "$(TEST_TOOBIB_DATABASE_ARGUMENTS)" -f "toobib-sample.sql"

drop-test-db:
	psql "$(TEST_PG_CONF_NO_DB)" -c "DROP DATABASE IF EXISTS $(TEST_DATABASE_NAME)"
