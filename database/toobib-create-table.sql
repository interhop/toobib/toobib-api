/* ******************************************************************************* */
/*                                                                                 */
/*                                                        ::::::::    :::::::::    */
/*   toobib-create-table.sql                            :+:    :+:   :+:    :+:    */
/*                                                     +:+    +:+   +:+    +:+     */
/*   By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+       */
/*                                                   +#+    +#+   +#+              */
/*   Created: 2023/01/21 20:19:54 by quentin        #+#    #+#   #+#               */
/*   Updated: 2023/04/09 13:29:37 by quentin        ########### ###                */
/*                                                                                 */
/* ******************************************************************************* */


DROP TABLE IF EXISTS users;

CREATE TABLE users (
	user_id 	INTEGER  NOT NULL, 
	username	TEXT, 
 	password       	TEXT,
 	phone       	TEXT,
	invalid_reason	TEXT,
	email		TEXT 
);


