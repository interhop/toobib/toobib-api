/* ******************************************************************************* */
/*                                                                                 */
/*                                                        ::::::::    :::::::::    */
/*   toobib-constraints.sql                             :+:    :+:   :+:    :+:    */
/*                                                     +:+    +:+   +:+    +:+     */
/*   By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+       */
/*                                                   +#+    +#+   +#+              */
/*   Created: 2023/01/21 20:19:40 by quentin        #+#    #+#   #+#               */
/*   Updated: 2023/01/21 21:14:45 by quentin        ########### ###                */
/*                                                                                 */
/* ******************************************************************************* */

ALTER TABLE users ADD CONSTRAINT xpk_users PRIMARY KEY (user_id);
CREATE SEQUENCE users_id_seq OWNED BY users.user_id;
ALTER TABLE users ALTER COLUMN user_id SET DEFAULT nextval('users_id_seq');
