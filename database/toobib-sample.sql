/* ******************************************************************************* */
/*                                                                                 */
/*                                                        ::::::::    :::::::::    */
/*   toobib-sample.sql                                  :+:    :+:   :+:    :+:    */
/*                                                     +:+    +:+   +:+    +:+     */
/*   By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+       */
/*                                                   +#+    +#+   +#+              */
/*   Created: 2023/01/21 20:25:03 by quentin        #+#    #+#   #+#               */
/*   Updated: 2023/01/28 18:20:03 by quentin        ########### ###                */
/*                                                                                 */
/* ******************************************************************************* */

/* unhashed password is 'password' */
INSERT INTO users(username, password, email, invalid_reason) VALUES ('user1', 'pbkdf2:sha256:260000$LXab69rgUEIXLVgJ$f7ce3ae1fcc60b66db11744e73af711b0bab369cc3c1c413ad6c56a6adbc72b7', 'email@email.com', 'V');
