# Installation et technologies utilisées

## Postgres

PostgreSQL est un gestionnaire de base de données relationnel opensource.

Nous avons choisi d'installer Postgres en local sur la machine (pas de déploiement  dans un container Docker).

### Installation de la base de données

- Prérequis : installation de PostgresSQL.

- Connexion à postgres
```bash
sudo -u postgres psql
```

- Création d'un user et d'un superuser qui permettra l'installation des extension psql. Dans notre cas le user est toobib et la db est toobib.

```psql
CREATE USER USER WITH PASSWORD 'PWD' CREATEDB;
CREATE ROLE USER_EXTENSION WITH LOGIN SUPERUSER PASSWORD 'PWD';
```

- Exit psql
```psql
\q
```

### Installation des données

- cd database

- Editer les macros  SCHEMA, USER, DB in Makefile si besoin.

- Créer un fichier [config](https://wiki.postgresql.org/wiki/Pgpass) ```.pgpass``` dans le home de l'utilisateur. Il doit être de la forme

```bash
"IP":"PORT":*:"USER":"PWD"
"IP":"PORT":*:"USER_EXTENSION":"PWD"
```

Exemple: 
```bash
localhost:5432:*:toobib:password
*:*:*:toobib_extension:password
```
- Appliquer les droits nécessaires au fichier ```.pgpass```
```bash
sudo chmod 600 ~/.pgpass
```

## Flask

Flask est un micro framework open-source de développement web en Python. Il est classé comme microframework car il est très léger. Flask a pour objectif de garder un noyau simple mais extensible.

### En développement

#### Avec Flask run

Au premier lancement utilisez la commande all du makefile se trouvant dans le dossier root.Cette commande executera l'installation de l'environnement virtuel et des dependances puis lancera les tests et en derniere instance servira l'API via Flask run sur le port choisi.

``` bash
	make all
```

Puis lors des lancements suivants il vous suffira de lancer la commande suivante:

``` bash
	make dev
```

#### Rechargement automatique en cas de modification du fichier source
Le serveur web de développement flask intégré à la capacité de se recharger automatiquement lorsqu'il détecte un changement dans un fichier source. Cela s'avère pratique pour les étapes de développement.
