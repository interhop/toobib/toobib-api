# Toobib-API

[Git repository](https://framagit.org/interhop/toobib/toobib-api)

# API Specs

--[API-swagger]()--

# Project

```
├── Makefile          				# make invokations
├── docs              				# markdown project documentation
│    ├── install				# installation documentation
│    │   └── index.md   			# installation description
│    └── index.md   				# Landing page
├── mkdocs.yml        				# documentation configuration
├── app-flask					# package
│    ├── tests					# tests
│    │   ├── conftest.py   			# pytest fixture
│    │   ├── data.sql   			# sample data
│    │   ├── setup.cfg   			# tests setup
│    │   ├── test\_postgres.py   		# postgres tests
│    │   ├── test\_users.py   			# user routes tests
│    │   └── \_\_init\_\_.py   			# needed for pytest to work
│    ├── server					
│    │   ├── blocklist.py   			# Blocklist initialization
│    │   ├── cache.py   			# Cache initialization
│    │   ├── db.py   				# Database initialization
│    │   ├── jwt.py   				# jwt decorations
│    │   ├── api   				# api
│    │   │    ├── models   			# ORM classes
│    │   │    │    └── internal\_models.py   	# ORM class of the user table
│    │   │    └── resources   			# endpoints
│    │   │         └── user.py   		# endpoint associated with user
│    │   ├── schema   				# required schema
│    │   │    ├── query   			# query schema 
│    │   │    │    └── user.py   		# schema of user routes
│    │   │    └── spec.py   			# spectree initialization
│    │   └── \_\_init\_\_.py   			# create\_app function
│    ├── database				# db
│    │   ├── Makefile				# make invokations for db
│    │   ├── toobib-create-table.sql		# create the table
│    │   ├── toobib-constraints.sql		# associate constraints to table
│    │   └── toobib-sample.sql   		# needed for pytest to work
│    └── requirements.txt			# dependencies	 
├── build-config-file.py      			# build the config file with secret keys 
└── .gitignore	       				# ignore selected file in git process 
```


