# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    config.py                                          :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:05:41 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/02/17 10:17:07 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

import os

class Config(object):
    SECRET_KEY = secret_key # paste here the value of the secret key

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True

    USER = 'toobib' 
    PWD = 'password' # paste here the same value of PWD declared in the Makefile
    DB = 'toobib'
    INTERNAL_DIRECTORY_SCHEMA = 'internal_toobib'

    POSTGRES = {
            'db': os.environ.get('POSTGRES_DB', DB),
            'user': os.environ.get('POSTGRES_USER', USER),
            'pwd': os.environ.get('POSTGRES_PWD', PWD),
            'host': os.environ.get('POSTGRES_HOST', 'localhost'),
            'port': os.environ.get('POSTGRES_PORT', '5432'),
            }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    PROPAGATE_EXCEPTIONS = True

    # JWT CONF
    JWT_SECRET_KEY = jwt_secret_key # paste here the value of the jwt secret key
    JWT_TOKEN_LOCATION = ['headers', 'query_string']    

    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False

class TestingConfig(Config):
    TESTING = True
    SESSION_COOKIE_SECURE = False
    USER_TEST = 'toobib' 
    PWD_TEST = 'password' # paste here the same value of PWD_TEST declared in the Makefile
    DB_TEST = 'toobib_test'
    INTERNAL_DIRECTORY_SCHEMA = 'internal_toobib'

    POSTGRES = {
            'db': os.environ.get('POSTGRES_DB_TEST', DB_TEST),
            'user': os.environ.get('POSTGRES_USER_TEST', USER_TEST),
            'pwd': os.environ.get('POSTGRES_PWD_TEST', PWD_TEST),
            'host': os.environ.get('POSTGRES_HOST', 'localhost'),
            'port': os.environ.get('POSTGRES_PORT', '5432'),
            }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s@%(host)s:%(port)s/%(db)s' % POSTGRES
