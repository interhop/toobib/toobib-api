# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    user.py                                            :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:04:38 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/04/09 13:42:24 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

import re
import werkzeug

from datetime import timedelta

from flask import request, Blueprint
from spectree import Response
from flask_jwt_extended import create_access_token, jwt_required, get_jwt
from server.schema.spec import spectree_api, user_tag
from server.schema.query.user import UserQuery
from server.api.models.internal_models import UserModel
from server.blocklist import BLOCKLIST

user = Blueprint("User", __name__)

@user.route("/login", methods=["POST"])
@spectree_api.validate(query=UserQuery, resp=Response("HTTP_404"), tags=[user_tag])
def Login():
    query = request.args.to_dict(flat=True)
    if 'username' not in query:
        return {'message': "Vous devez renseigner un nom d'utilisateur"}, 404
    if 'password' not in query:
        return {'message': 'Vous devez renseigner un mot de passe'}, 404
    user = UserModel.find_by_username(query['username'])
    print(user)
    if user and user.check_password(query['password']) and user.invalid_reason != 'D': #TODO: method user_is_valid()
        expires = timedelta(days=1)
        access_token = create_access_token(identity=user, fresh=True, expires_delta=expires)
        return {
                'access_token': access_token,
                'user': user.registered(),
        }, 200
    return {"message": "Identifiants invalides"}, 401

@user.route("/register", methods=["POST"])
@spectree_api.validate(query=UserQuery, resp=Response("HTTP_200"), tags=[user_tag])
def Register():
    query = request.args.to_dict(flat=True)
    if 'password' not in query:
        return {'message': 'Vous devez renseigner un mot de passe'}, 404
    if 'email' not in query or re.fullmatch("([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+", query['email']) == None:
        return {'message': 'Vous devez renseigner une adresse mail valide'}, 404
    user = UserModel.find_by_username(query['email'])
    if user:
        return {
                'message': "Le adresse mail est déjà utilisée"
                }, 401
    user = UserModel(query['username'], query['password'], query['email'])
    user.upserting()
    return user.registered()


@user.route("/logout", methods=["POST"])
@spectree_api.validate(resp=Response("HTTP_200"), security={"auth_BearerAuth": []
    }, tags=[user_tag])
@jwt_required()
def Logout():
    jti = get_jwt()['jti']
    BLOCKLIST.add(jti)
    return {"message": "Successfully logged out"}, 200

@user.route("/update", methods=["POST"])
@spectree_api.validate(query=UserQuery, resp=Response("HTTP_200"), security={"auth_BearerAuth": [] }, tags=[user_tag])
@jwt_required()
def UpdateUser():
    jti = get_jwt()['jti']
    query = request.args.to_dict(flat=True)
    if 'email' not in query:
        return {'message': 'Vous devez renseigner une adresse mail valide'}, 404
    user = UserModel.find_by_email(query['email'])
    if not user:
        return {'message': 'Vous devez renseigner une adresse mail valide'}, 404
    user.update(query)
    return user.json()

        

