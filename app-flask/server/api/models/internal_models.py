# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    internal_models.py                                 :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/21 20:13:30 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/04/09 13:35:42 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

from werkzeug.security import generate_password_hash, \
        check_password_hash

from server.config import Config
from server.db import db

class UserModel(db.Model):
    __tablename__ = 'users'
    __table_args__ = ({'schema': Config.INTERNAL_DIRECTORY_SCHEMA})

    id = db.Column('user_id', db.Integer,
        primary_key=True, autoincrement=True)
    username = db.Column(db.Text())
    password = db.Column(db.Text())
    phone = db.Column(db.Text())
    email = db.Column(db.Text())
    invalid_reason = db.Column(db.Text())

    def __init__(self, username, password, email):
        self.username = username
        self.set_password(password)
        self.email = email 
    
    def json(self):
        return {
            '_id': self.id,
            'username': self.username,
            'email': self.email,
            }

    def registered(self):
        return {
            'ressource': "login",
            '_id': self.id,
            'username': self.username,
            'email': self.email,
        }

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first() 

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first() 
    
    def set_password(self, password):
        self.password = generate_password_hash(password, method='pbkdf2:sha256', salt_length=8)

    def check_password(self, password):
        return check_password_hash(self.password, password)
    
    def update(self, data):
        user = UserModel.find_by_email(self.email)
        if data.get('username'):
            user.username = data['username']
        user.upserting()


    def upserting(self):
        db.session.add(self)
        db.session.commit()
