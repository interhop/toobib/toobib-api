# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    jwt.py                                             :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:06:00 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/02/11 18:24:43 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

from flask_jwt_extended import JWTManager

from server.blocklist import BLOCKLIST

jwt = JWTManager()

@jwt.additional_claims_loader
def add_claims_to_access_token(user):
    return{
            'username':user.username,
            'email': user.email,
            }

@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.id

@jwt.token_in_blocklist_loader
def check_if_token_in_blocklist(jwt_header, jwt_payload):
    return jwt_payload['jti'] in BLOCKLIST


