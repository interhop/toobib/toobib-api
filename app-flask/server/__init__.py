# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    __init__.py                                        :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:06:13 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/02/09 18:39:37 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

import os
import logging

from flask import Flask
from flask_cors import CORS

from server.blocklist import BLOCKLIST
from server.cache import cache
from server.config import DevConfig
from server.db import db
from server.jwt import jwt
from server.api.resources.user import user
from server.schema.spec import spectree_api

def create_app(test_config=None):
    # Launch the app and the cache system
    app = Flask(__name__, instance_relative_config=True)
    cache.init_app(app, config={'CACHE_TYPE':'SimpleCache'})
    
    # load correct configuration
    if test_config is None:
        app.config.from_object(DevConfig())
    else:
        # app.config.from_mapping(test_config)
        app.config.from_object(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    logging.basicConfig(filename='error.log', level=logging.DEBUG)
    db.init_app(app)
    cors = CORS(app, resources={r"/foo": {"origins": "*"}})
    app.config['CORS_HEADERS'] = 'Content-Type'
    print(app.config['SQLALCHEMY_DATABASE_URI'])
    jwt.init_app(app)

    app.register_blueprint(user)
    spectree_api.register(app)

    return app
