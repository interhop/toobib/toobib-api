# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    user.py                                            :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:08:54 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/01/28 19:09:14 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

from enum import Enum
from typing import List, Optional
from pydantic import BaseModel, Field

class UserQuery(BaseModel):
        username: Optional[str] = Field(alias="username", default="username", description="The username associated with the resource.")
        email: Optional[str] = Field(alias="email", default="email@email.fr", description="The email associated with the resource.")
        password: Optional[str] = Field(default="password", description="The password that identifies the code system")

