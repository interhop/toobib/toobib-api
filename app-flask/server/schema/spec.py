# ********************************************************************************* #
#                                                                                   #
#                                                         ::::::::    :::::::::     #
#    spec.py                                            :+:    :+:   :+:    :+:     #
#                                                      +:+    +:+   +:+    +:+      #
#    By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+        #
#                                                    +#+    +#+   +#+               #
#    Created: 2023/01/28 19:08:24 by quentin        #+#    #+#   #+#                #
#    Updated: 2023/02/11 18:08:11 by quentin        ########### ###                 #
#                                                                                   #
# ********************************************************************************* #

from spectree import SpecTree, SecurityScheme, Tag

security_schemas = [
    SecurityScheme(name="auth_BearerAuth", data={
                       "type": "http", "scheme": "bearer", "description": "Fill the field with 'access_token' from the response of the login request."}),
]

user_tag = Tag(
            name="User", description="The User resource describes a person register on the service.",
            externalDocs={"url": "https://framagit.org/interhop/toobib", "description": "Documentation"})


spectree_api = SpecTree("flask", version='v1.0', security_schemes=security_schemas)
