import os
import tempfile
from flask import Flask, json
from flask_sqlalchemy import SQLAlchemy
import pytest
from server import create_app

def test_login_client(app):
    client = app.test_client()
    user1 = {
            'username': 'user1',
            'password': 'password'
            }
    response = client.post(f"/login?username={user1['username']}&password={user1['password']}")
    print(response.data)
    data = json.loads(response.data)
    assert response.status_code == 200
    assert 'user1' == data["user"]['username']

def test_register_user(app):
    client = app.test_client()
    response = client.post('/register?username=testuser&email=testemail%40email.fr&password=testpassword')
    data = json.loads(response.data)
    print(data)
    assert response.status_code == 200
    assert  'testemail@email.fr' == data["email"]
    assert  'testuser' == data["username"]
    assert  'login' == data["ressource"]

def test_already_registered_user(app):
    client = app.test_client()
    response = client.post('/register?username=testuser&email=testemail%40email.fr&password=testpassword')
    data = json.loads(response.data)
    print(data)
    assert response.status_code == 401
    assert  "Le nom d'utilisateur est déjà utilisé" == data["message"]

def test_register_user_with_wrong_email(app):
    client = app.test_client()
    user1 = {
            'username': 'user1',
            'email': 'email',
            'password': 'password'
            }
    response = client.post(f"/register?username={user1['username']}&email={user1['email']}&password={user1['password']}")
    print(response.data)
    data = json.loads(response.data)
    assert response.status_code == 404
    assert 'Vous devez renseigner une adresse mail valide' == data["message"]

def test_login_new_user(app):
    client = app.test_client()
    response = client.post(f"/login?username=testuser&email=testemail%40email.fr&password=testpassword")
    data = json.loads(response.data)

def test_login_and_logout_new_user(app):
    client = app.test_client()
    login = client.post(f"/login?username=testuser&email=testemail%40email.fr&password=testpassword")
    login_data = json.loads(login.data)
    assert login.status_code == 200
    assert 'testuser' == login_data["user"]['username']
    assert  'testemail@email.fr' == login_data["user"]["email"]
    assert  'testuser' == login_data["user"]["username"]
    assert  'login' == login_data["user"]["ressource"]
    token = login_data["access_token"]
    response = client.post(f"/logout", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(response.data)
    assert response.status_code == 200
    assert 'Successfully logged out' == data["message"]

def test_addinfo_no_token(app):
    token = ""
    response = client.post(f"/userinfo?email=testemail%40email.fr&rpps=10000001916", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(response.data)
    assert response.status_code == 401
    assert 'Unauthorized' == data["message"]

def test_add_info(app):
    client = app.test_client()
    login = client.post(f"/login?username=testuser&email=testemail%40email.fr&password=testpassword")
    login_data = json.loads(login.data)
    assert login.status_code == 200
    token = login_data["access_token"]
    response = client.post(f"/userinfo?email=testemail%40email.fr&rpps=10000001916", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(response.data)
    assert response.status_code == 200
    assert 'testemail@email.fr' == data["Practitioner"]["telecom"][0]["value"]
    assert '10000001916' == data["Practitioner"]["Identifier"][0]["value"]

