{
	"fullUrl": "https://gateway.api.esante.gouv.fr/fhir/v1/Practitioner/003-137410",
	"resource": {
		"resourceType": "Practitioner",
		"id": "003-137410",
		"meta": {
			"versionId": "1",
			"lastUpdated": "2023-01-24T01:49:14.876+00:00",
			"source": "https://annuaire.sante.fr",
                        "profile": [
                            "https://annuaire.sante.gouv.fr/fhir/StructureDefinition/AS-Practitioner"
                        ]
                },
                "language": "fr",
                "identifier": [
                    {
                        "use": "official",
                        "type": {
                            "coding": [
                                {
                                    "system": "http://interopsante.org/CodeSystem/v2-0203",
                                    "code": "IDNPS"
                                }
                            ]
                        },
                        "system": "urn:oid:1.2.250.1.71.4.2.1",
                        "value": "810000001916"
                    },
                    {
                        "use": "official",
                        "type": {
                            "coding": [
                                {
                                    "system": "http://interopsante.org/CodeSystem/v2-0203",
                                    "code": "RPPS"
                                }
                            ]
                        },
                        "system": "http://rpps.fr",
                        "value": "10000001916"
                    }
                ],
                "active": true,
                "name": [
                    {
                        "prefix": [
                            "M"
                        ]
                    }
                ],
                "qualification": [
                    {
                        "code": {
                            "coding": [
                                {
                                    "system": "https://mos.esante.gouv.fr/NOS/TRE_R48-DiplomeEtatFrancais/FHIR/TRE-R48-DiplomeEtatFrancais",
                                    "code": "DE05"
                                },
                                {
                                    "system": "https://mos.esante.gouv.fr/NOS/TRE_R14-TypeDiplome/FHIR/TRE-R14-TypeDiplome",
                                    "code": "DE"
                                }
                            ]
                        }
                    }
                ]
            }
}
