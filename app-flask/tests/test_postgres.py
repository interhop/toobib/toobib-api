import os
import pprint as pp

import pytest
from sqlalchemy import text
from server.db import db

#@pytest.mark.skipif(
#    os.environ.get("GITLAB_CI") == "true", reason="cannot download container on CI"
#)

def test_connection_postgres(app):
    app.app_context().push()
    with db.engine.connect() as conn:
        res = conn.execute(text("SELECT 1")).first()
        assert res == (1,)

def test_expected_database(app):
    app.app_context().push()
    with db.engine.connect() as conn:
        res = conn.execute(text("SELECT current_database()")).first()
        assert res == ('toobib_test',)

def test_expected_tables(app):
    app.app_context().push()
    with db.engine.connect() as conn:
        res = conn.execute(text("SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';")).first()
        assert res == ('users',)

