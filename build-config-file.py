import random, string, re
secret_key = "".join([random.choice(string.digits + string.ascii_lowercase + string.ascii_uppercase + string.punctuation) for _ in range(24)]).replace("'", '').replace("\\", '')
jwt_secret_key = "".join([random.choice(string.digits + string.ascii_lowercase + string.ascii_uppercase + string.punctuation) for _ in range(24)]).replace("'", '').replace("\\", '')

with open("app-flask/server/config_sample.py", "rt") as fin:
    with open("app-flask/server/config.py", "wt") as fout:
        for line in fin:
            if line != line.replace('jwt_secret_key', jwt_secret_key):
                fout.write(line.replace('jwt_secret_key', "'" + jwt_secret_key + "'"))
            else:
                fout.write(line.replace('secret_key', "'" + secret_key + "'"))
